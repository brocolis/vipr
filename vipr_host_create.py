#!/usr/bin/python
import sys
import time
import json
import getpass
import pexpect
from subprocess import check_output


def auth(user):
    password = getpass.getpass()
    child = pexpect.spawn(
        '{0} -hostname {1} authenticate -u {2} -d /tmp -cf {3}'.format(vipr_path, ip_viprold, user, cf_viprold))
    child.expect('Password : ')
    child.sendline(password)
    time.sleep(1)
    child = pexpect.spawn(
        '{0} -hostname {1} authenticate -u {2} -d /tmp -cf {3}'.format(vipr_path, ip_viprnew, user, cf_viprnew))
    child.expect('Password : ')
    child.sendline(password)
    time.sleep(1)
    return


def non_discoverable_hosts(host_list):
    l = []
    for host in host_list:
        if not host['discoverable'] != False:
          l.append(host['name'])
    return l


def unregistered_hosts_list(old_list, new_list):
    l = []
    for host in old_list:
        if host not in new_list:
            l.append(host)
    return l


def register_hosts(old_host_list, unregist_hosts_list):
    for host in old_host_list:
        if host['name'] in unregist_hosts_list:
            try:
                initiator = json.loads(
                    check_output(
                        ['{0} host list-initiators -hl {1} -verbose -cf /tmp/{2} -hostname {3}'
                             .format(vipr_path, host['name'], cf_viprold, ip_viprold)], shell=True))
            except ValueError:
                continue
            print(host['name'])
            child = pexpect.spawn(
                '{0} host create -hn {1} -type {2} -hl {3} -hp {4} -un spvipr01@prbbr -cf /tmp/{5} -hostname {6} '
                '-discover false'.format(vipr_path, host['host_name'], host['type'], host['name'], host['port_number'],
                                         cf_viprnew, ip_viprnew))
            child.expect('Enter password of the host: ')
            child.sendline('password')
            child.expect('Retype password: ')
            child.sendline('password')
            time.sleep(1)
            child.close()
            for init in initiator:
                print(init['initiator_port'])
                if init['protocol'] == 'FC':
                    check_output(
                        ['{0} initiator create -hl {1} -protocol FC -initiatorwwn {2} -initiatorportwwn {3} '
                         '-cf /tmp/{4} -hostname {5}'.format(vipr_path, host['name'], init['initiator_node'],
                                                             init['initiator_port'], cf_viprnew,
                                                             ip_viprnew)], shell=True)
                else:
                    continue


if __name__ == "__main__":
    try:
        cf_viprold = 'oldvipr.corpcookie'
        cf_viprnew = 'newvipr.corpcookie'
        ip_viprold = '22.161.35.192'
        ip_viprnew = '22.161.35.10'
        vipr_path = '/opt/storageos/cli/bin/viprcli-3.6.1.1-py2.6.egg/viprcli/viprcli'
        user = sys.argv[1]
        auth(user)
        print('Querying Old ViPR Database ...')
        hostlist_viprold = json.loads(
            check_output(['{0} host list -verbose -cf /tmp/{1} -hostname {2}'
                         .format(vipr_path, cf_viprold, ip_viprold)], shell=True))
        print('Querying New ViPR Database ...')
        hostlist_viprnew = json.loads(
            check_output(['{0} host list -verbose -cf /tmp/{1} -hostname {2}'
                         .format(vipr_path, cf_viprnew, ip_viprnew)], shell=True))
        vipro_list = non_discoverable_hosts(hostlist_viprold)
        viprn_list = non_discoverable_hosts(hostlist_viprnew)
        host_list_name = unregistered_hosts_list(vipro_list, viprn_list)
        register_hosts(hostlist_viprold, host_list_name)
    except KeyboardInterrupt:
            exit()